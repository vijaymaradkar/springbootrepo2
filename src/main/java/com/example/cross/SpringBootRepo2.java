package com.example.vijay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRepo2 {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRepo2.class, args);
	}
}
